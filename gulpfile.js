var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    cmq = require('gulp-combine-media-queries'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload;

// processing sass into css - 'gulp styles'
gulp.task('styles', function() {
    return sass('css/style.scss', {
        style: 'expanded',
        sourcemap: true
    })
    .pipe(autoprefixer('last 3 version'))
    .on('error', function (err) { console.log(err.message); })
    .pipe(sourcemaps.write())
    .pipe(cmq({
      log: true
    }))
    .pipe(gulp.dest('css'))
    .pipe(reload({stream: true}))
    .pipe(notify({ message: 'Styles task complete' }));
});

// listening for changes to scss and images - 'gulp watch'
gulp.task('watch', function() {
    gulp.watch('css/**/*.scss', ['styles']);
});

// live reload via browser-sync - 'gulp serve'
gulp.task('serve', function() {
    browserSync.init({ proxy: "YOUR.STAGING.URL" });
    gulp.watch("css/**/*.scss", ['styles']);
    gulp.watch("**/*.php").on('change', reload);
});

// default tasks enacted by typing 'gulp'
gulp.task('default', function() {
    gulp.start('styles');
});